#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Code from MCMI Blog Export

import sys
import urllib2
import os
from xml.dom import minidom
from xml.etree import ElementTree as ET
from BeautifulSoup import BeautifulSoup  # refactoring with tutorial?
Soup = BeautifulSoup


import BeautifulSoup 
import re

import HTMLParser
pars = HTMLParser.HTMLParser()


from HTMLParser import HTMLParser

reload(sys)
sys.setdefaultencoding('utf8')

# Used on CNPfeedWriter

import datetime
import PyRSS2Gen

# For datetime calls (http://stackoverflow.com/a/10801903):

import locale
locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")


'''
http://www.crummy.com/software/BeautifulSoup/bs4/doc/

sudo apt-get install python-bs4
'''

class Reader():    
    
    def __init__(self,url_or_filename):
        self.listcomments = []
           
        try:
            #self.soup = Soup( urllib2.urlopen(url_or_filename) )
            self.soup = Soup( open(url_or_filename) )
        except KeyboardInterrupt:
            print 'Aborted'
            sys.exit(0)
        except Exception, e:
            print e
            sys.exit(1)      


    def execute(self):
        
        attrs = {'class' : re.compile("data-list")}
        table = self.soup.findAll('table', attrs)[0]
        trs = table.findAll('tr')[1:]  # exclude tr with th
        
        '''
        <tr><td class="col01"><a href="/episodios/o-canto-da-paz-e-liturgico">162</a></td><td class="col02"><a href="/episodios/o-canto-da-paz-e-liturgico">O canto da paz é litúrgico?</a></td><td class="col03"><a href="/episodios/o-canto-da-paz-e-liturgico">08:47</a></td><td class="col04"><a href="/episodios/o-canto-da-paz-e-liturgico">Abril 01, 2013</a></td></tr>
        '''
        
        # path
        # num
        # title
        # duration
        # date
        
        data = []
        
        for tr in trs:
            tds = tr.findAll('td')

            path = tds[0].a['href']  # equal in all
            num = tds[0].a.string
            title = tds[1].a.string
            duration = tds[2].a.string
            date = tds[3].a.string
            
            e = Entry(path, num, title, duration, date)
            data.append(e)
        
        return data


class Entry():
      
    def __init__(self, path, num, title, duration, date):
        self.path = path
        self.num = num
        self.title = title
        self.duration = duration
        self.date = date
    
    
    # http://padrepauloricardo.org    
    # /episodios/qual-a-situacao-dos-casais-em-segunda-uniao-parte-ii
    def getURL(self, prefix):
        return "%s%s" % (prefix, self.path)
    
    def getDurationForHuman(self):
        #print self.title, self.duration
        if self.duration == None:
            return "Sem estimativa de tempo".encode('iso-8859-1')
        if self.duration == "----":
            return "Sem áudio".encode('iso-8859-1')
        parts = self.duration.split(":")        
        if len(parts) == 3:        
            #return "%s horas, %s minutos e % segundos" % (
            #    parts[0], parts[1], parts[2])
            hours = int(parts[0])
            minutes = int(parts[1])
            seconds = int(parts[2])            
            return "{hours} hora{phours}, " \
                "{minutes} minuto{pminutes} e " \
                "{seconds} segundo{pseconds}".format(
                    hours = hours,
                    phours = "" if hours == 1 else "s",
                    minutes = minutes, 
                    pminutes = "" if minutes == 1 else "s",
                    seconds = seconds,
                    pseconds = "" if seconds == 1 else "s"                    
            ).encode('iso-8859-1')
        else:  # is 2
            #return "%s minutos e %s segundos" % (parts[0], parts[1])
            minutes = int(parts[0])
            seconds = int(parts[1])
            return "{minutes} minuto{pminutes} e " \
                "{seconds} segundo{pseconds}".format(
                    minutes = minutes, 
                    pminutes = "" if minutes == 1 else "s",
                    seconds = seconds,
                    pseconds = "" if seconds == 1 else "s"
            ).encode('iso-8859-1')
        
    def getDatetime(self):
        import string
        #toParse = string.lower("Abril 26, 2011")
        toParse = string.lower(self.date)
        return datetime.datetime.strptime(toParse, "%B %d, %Y")

        
    def getTitleNumered(self):
        return "%s - %s" % (self.num, self.title)
          
  
'''
http://www.dalkescientific.com/Python/PyRSS2Gen.html

sudo apt-get install python-pyrss2gen 
'''
    
class CNPfeedWriter():

    def __init__(self,
        title = "Title of feed test",
        link = "http://www.dalkescientific.com/Python/PyRSS2Gen.html",
        description = "The latest news about PyRSS2Gen, a ",
        outputFilename = "pyrss2gen.xml",
        prefix = "http://padrepauloricardo.org"):
        
        self.title = title.encode('iso-8859-1')
        self.link = link.encode('iso-8859-1')
        self.description = description.encode('iso-8859-1')
        
        self.filename = outputFilename        
        self.prefix = prefix

        
    def __itemsFor__(self, entries):
        items = []
        for e in entries:
            item = PyRSS2Gen.RSSItem(
                title = e.getTitleNumered(),
                link = e.getURL(self.prefix),
                description = e.getDurationForHuman(),
                guid = e.getURL(self.prefix),
                pubDate = e.getDatetime()
            )
            items.append(item)
        return items
    
    
    def makeWith(self, data):
        lastBuildDate = datetime.datetime.now()
        items = self.__itemsFor__(data)
        
        rss = PyRSS2Gen.RSS2(
            title = self.title,
            link = self.link,
            description = self.description,
            lastBuildDate = lastBuildDate,
            items = items
        )
        
        rss.write_xml(open(self.filename, "w"))
    
    

if (len(sys.argv) > 1):
    reader = Reader(sys.argv[1])
    reader.execute()
    #reader.test()
    #? reader.html_post()
else:
    #url = "http://padrepauloricardo.org/programas/a-resposta-catolica"

    limit = 20
    limit = limit - 1
    #limit = None    
 
    data = Reader("cache/a-resposta-catolica.htm").execute()[0:limit]    
    CNPfeedWriter(title = "Padre Paulo Ricardo - A Resposta Católica",
        link = "http://padrepauloricardo.org/programas/a-resposta-catolica",
        description = '''Programa semanal dedicado a responder às perguntas de alunos e visitantes do site padrepauloricardo.org, utilizando sempre a doutrina da Igreja Católica sobre os mais diversos assuntos. Tem como objetivo ajudar o fiel a viver mais plenamente a sua fé e conhecê-la mais profundamente.''',
        outputFilename = "rss/a-resposta-catolica.xml"
    ).makeWith(data)

    data = Reader("cache/parresia.htm").execute()[0:limit]    
    CNPfeedWriter(title = "Padre Paulo Ricardo - Parresía",
        link = "http://padrepauloricardo.org/programas/parresia",
        description = '''O sentido de parresía remete à coragem, ao destemor de dizer a verdade, mesmo sob pena de ser condenado por isso. É uma característica dos Apóstolos de Cristo logo após terem recebido o Espírito Santo. Deve ser, portanto, uma marca de todo católico, confirmado em sua fé.

Aplicando este conceito, Padre Paulo Ricardo trata com franqueza e coragem os assuntos mais relevantes do momento, sempre com o intuito de ajudar os cristãos católicos a viverem com mais ardor e determinação a sua fé.''',
        outputFilename = "rss/parresia.xml"
    ).makeWith(data)

    data = Reader("cache/testemunho-de-fe.htm").execute()[0:limit]    
    CNPfeedWriter(title = "Padre Paulo Ricardo - Testemunho de Fé",
        link = "http://padrepauloricardo.org/programas/testemunho-de-fe",
        description = '''A Palavra do Senhor é alimento para a vida do cristão católico. Cada um é como um terreno arado e a Palavra é como a semente. Unidos geram a vida e produzem frutos.

Testemunho de Fé é o programa semanal em que o Padre Paulo Ricardo faz um comentário exegético-espiritual das leituras da Liturgia Dominical. É uma fonte de aprofundamento no conhecimento da Palavra proclamada na Santa Missa.

É também uma forma a mais de preparar a terra do seu coração para receber a semente que Deus quer plantar.''',
        outputFilename = "rss/testemunho-de-fe.xml"
    ).makeWith(data)

    data = Reader("cache/aula-ao-vivo.htm").execute()[0:limit]    
    CNPfeedWriter(title = "Padre Paulo Ricardo - Aula ao Vivo",
        link = "http://padrepauloricardo.org/programas/aula-ao-vivo",
        description = '''Todas as terças feiras, às 21h00 (horário de Brasília), acontecerá a Aula ao Vivo. Este programa tem como objetivo principal proporcionar ao aluno e assinante do site padrepauloricardo.org uma maior proximidade com o padre Paulo Ricardo. A aula é dividida em duas partes. Na primeira, o padre comentará sobre temas atuais da Igreja e, na segunda, responderá às perguntas dos alunos, que poderão utilizar o chat e falar com a equipe em tempo real.

Algumas Aulas ao Vivo serão abertas ao público geral e suas gravações estarão disponíveis no site também para os não assinantes.''',
        outputFilename = "rss/aula-ao-vivo.xml"
    ).makeWith(data)
