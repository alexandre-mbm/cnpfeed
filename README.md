Repositório Git para acesso por SSH:
```console
$ ls -la ~/repos/
total 4
drwxrwxr-x  2 alexandre.mbm alexandre.mbm   32 May  5  2013 .
drwxrwx--- 14 alexandre.mbm alexandre.mbm 4096 Sep  3 15:15 ..
lrwxrwxrwx  1 alexandre.mbm alexandre.mbm   15 May  5  2013 CNPfeed.git -> ../www/CNPfeed/
```
Resultado de `crontab -l`:
```ini
TZ=America/Fortaleza

# A configuração acima funciona para dentro dos programas executados.
# Por exemplo, um comando 'date' passa a informa no fuso horário BRT.
# Porém, a configuração do trabalho abaixo não baseia-se nele. Então,
# 45 19 será 19h45min no fuso horário CEST, 14h45min no fuso BRT.
#
# 19:45h, horário Brasília = 00:45h do dia seguinte, horário Paris
#
# Fonte: http://www.calculoexato.com.br/conversoes/fusos

# m h  dom mon dow   command
#45 0 *  *  *  /home/alexandre.mbm/www/CNPfeed/job.sh
```
