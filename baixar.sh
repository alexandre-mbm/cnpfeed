#!/bin/bash

wget --tries=10 \
     "https://padrepauloricardo.org/programas/a-resposta-catolica" \
     --no-check-certificate \
     -O "cache/a-resposta-catolica.htm"
     
wget --tries=10 \
     "https://padrepauloricardo.org/programas/parresia" \
     --no-check-certificate \
     -O "cache/parresia.htm"
 
wget --tries=10 \
     "https://padrepauloricardo.org/programas/testemunho-de-fe" \
     --no-check-certificate \
     -O "cache/testemunho-de-fe.htm"

wget --tries=10 \
     "https://padrepauloricardo.org/programas/programa-ao-vivo" \
     --no-check-certificate \
     -O "cache/aula-ao-vivo.htm"
